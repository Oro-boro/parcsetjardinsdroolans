<?php

namespace App\Tests;

use App\Entity\Images;
use App\Entity\Services;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ImagesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $images = new Images();
        $services = new Services();
        $user = new User();
        $datetime = new Datetime();

        $images
            ->setNom('nom')
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setEnImages(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addService($services)
            ->setUser($user);

        $this->assertTrue($images->getNom() === 'nom');
        $this->assertTrue($images->getCreatedAt() === $datetime);
        $this->assertTrue($images->getDescription() === 'description');
        $this->assertTrue($images->getEnImages() === true);
        $this->assertTrue($images->getSlug() === 'slug');
        $this->assertTrue($images->getFile() === 'file');
        $this->assertContains($services, $images->getServices());
        $this->assertTrue($images->getUser() === $user);
    }

    public function testIsFalse()
    {
        $images = new Images();
        $services = new Services();
        $user = new User();
        $datetime = new Datetime();

        $images
            ->setNom('nom')
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setEnImages(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addService($services)
            ->setUser($user);

        $this->assertFalse($images->getNom() === 'false');
        $this->assertFalse($images->getCreatedAt() === new datetime());
        $this->assertFalse($images->getDescription() === 'false');
        $this->assertFalse($images->getEnImages() === false);
        $this->assertFalse($images->getSlug() === 'false');
        $this->assertFalse($images->getFile() === 'false');
        $this->assertNotContains(new services(), $images->getServices());
        $this->assertFalse($images->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $images = new Images();

        $this->assertEmpty($images->getId());
        $this->assertEmpty($images->getNom());
        $this->assertEmpty($images->getCreatedAt());
        $this->assertEmpty($images->getDescription());
        $this->assertEmpty($images->getEnImages());
        $this->assertEmpty($images->getSlug());
        $this->assertEmpty($images->getFile());
        $this->assertEmpty($images->getServices());
        $this->assertEmpty($images->getUser());
    }
}
