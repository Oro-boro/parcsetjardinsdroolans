<?php

namespace App\Tests;

use App\Entity\Images;
use App\Entity\Services;
use PHPUnit\Framework\TestCase;

class ServicesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $Services = new Services();
        $Services
            ->setNom('nom')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertTrue($Services->getNom() === 'nom');
        $this->assertTrue($Services->getDescription() === 'description');
        $this->assertTrue($Services->getSlug() === 'slug');
    }

    public function testIsFalse()
    {
        $Services = new Services();
        $Services
            ->setNom('nom')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertFalse($Services->getNom() === 'false');
        $this->assertFalse($Services->getDescription() === 'false');
        $this->assertFalse($Services->getSlug() === 'false');
    }

    public function testIsEmpty()
    {
        $Services = new Services();

        $this->assertEmpty($Services->getNom());
        $this->assertEmpty($Services->getDescription());
        $this->assertEmpty($Services->getSlug());
        $this->assertEmpty($Services->getId());
    }

    public function testRemoveAddImagesServices()
    {
        $services = new Services();
        $images = new Images();

        $this->assertEmpty($services->getImages());

        $services->addImage($images);
        $this->assertContains($images, $services->getImages());

        $services->removeImage($images);
        $this->assertEmpty($services->getImages());
    }
}
