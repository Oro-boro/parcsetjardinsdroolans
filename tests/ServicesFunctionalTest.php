<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServicesFunctionalTest extends WebTestCase
{
    public function testDisplayServices()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/services');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Création');
    }

    public function testDisplayOneService()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/services/service-TEST');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'service TEST');
    }
}
