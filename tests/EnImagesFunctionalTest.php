<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EnImagesFunctionalTest extends WebTestCase
{
    public function testDisplayEnImages()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en-images');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'En images');
    }
}
