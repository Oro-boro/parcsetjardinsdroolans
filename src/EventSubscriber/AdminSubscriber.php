<?php

namespace App\EventSubscriber;

use DateTime;
use App\Entity\Images;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class AdminSubscriber implements EventSubscriberInterface
{
    private $slugger;
    private $security;

    public function __construct(SluggerInterface $slugger, Security $security)
    {
        $this->slugger = $slugger;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ["setDateAndUser"]
        ];
    }

    public function setDateAndUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Images)) {
            return;
        }

        $now = new DateTime('now');
        $entity->setCreatedAt($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }

    /* -------------->   AU CAS OU : DON'T FORGET !!
    public function setDateAndUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (($entity instanceof Images)) {
            $now = new DateTime('now');
            $entity->setCreatedAt($now);
            $user = $this->security->getUser();
            $entity->setUser($user);
        }
        if (($entity instanceof Images)) {
            $now = new DateTime('now');
            $entity->setCreatedAt($now);
            $user = $this->security->getUser();
            $entity->setUser($user);
        }
        return;
    }
    */
}
