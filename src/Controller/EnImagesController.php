<?php

namespace App\Controller;

use App\Repository\ImagesRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EnImagesController extends AbstractController
{
    /**
     * @Route("/en-images", name="en-images")
     */
    public function enImages(
        ImagesRepository $imagesRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $imagesRepository->findBy([], ['id' => 'DESC']);
        $images = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            4
        );

        return $this->render('en-images/en-images.html.twig', [
            'images' => $images,
        ]);
    }
}
