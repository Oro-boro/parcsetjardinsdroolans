<?php

namespace App\Controller;

use App\Entity\Services;
use App\Repository\ImagesRepository;
use App\Repository\ServicesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServicesController extends AbstractController
{
    /**
     * @Route("/services", name="services")
     */
    /*
    public function services(): Response
    {
        return $this->render('services/services.html.twig', [
            'controller_name' => 'ServicesController',
        ]);
    }
    */

    /**
     * @Route("/services", name="services")
     */
    public function services(ServicesRepository $servicesRepository): Response
    {
        return $this->render('services/services.html.twig', [
            'services' => $servicesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/services/{slug}", name="serviceBySlug")
     */
    public function serviceBySlug(Services $service, ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findAllEnimage($service);

        return $this->render('services/creation/creation.html.twig', [
            'service' => $service,
            'images' => $images,
        ]);
    }

    /**
     * @Route("/services/creation", name="creation")
     */
    /*
    public function creation(ImagesRepository $imagesRepository): Response
    {
        return $this->render('services/creation/creation.html.twig', [
            'images' => $imagesRepository->serviceCreationImages(),
        ]);
    }
    */
}
