<?php

namespace App\Repository;

use App\Entity\Images;
use App\Entity\Services;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Images|null find($id, $lockMode = null, $lockVersion = null)
 * @method Images|null findOneBy(array $criteria, array $orderBy = null)
 * @method Images[]    findAll()
 * @method Images[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Images::class);
    }

    /**
     * @return Images[] Returns an array of Images objects
     */
    public function serviceCreationImages()
    {
        return $this->createQueryBuilder('i')
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Images[] Returns an array of Images objects
     */
    public function findAllEnimage(Services $service): array
    {
        return $this->createQueryBuilder('s')
            ->where(':service MEMBER OF s.services')
            ->andWhere('s.enImages = TRUE')
            ->setParameter('service', $service)
            ->getQuery()
            ->getResult();
    }
}
