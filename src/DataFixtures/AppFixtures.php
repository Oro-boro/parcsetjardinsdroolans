<?php

namespace App\DataFixtures;

use App\Entity\Images;
use App\Entity\Services;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //insertion de faker
        $faker = Factory::create('fr_FR');

        //insert utilisateur
        $user = new User();
        $adminRole = ['ROLE_ADMIN'];
        $user
            ->setEmail('User@test.com')
            ->setNom($faker->lastname())
            ->setPrenom($faker->firstname())
            ->setFacebook('facebook')
            ->setRoles($adminRole);

        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);
        $manager->persist($user);
        $manager->flush();

        //insert des services (A des fins de test de DB! Pour rappel, les services sont DEJA clairs et definis :
        // "Création", "Entretien", "Aménagements" et "Abattage et Elagage"
        // Le client pourra ajouter des images dans le service désiré

        //création de 4 services
        for ($s = 0; $s < 4; $s++) {
            $services = new Services();
            $services
                ->setNom($faker->word())
                ->setDescription($faker->words(10, true))
                ->setSlug($faker->slug());

            $manager->persist($services);

            //insert de 3 images PAR services (Donc 12 images)
            for ($i = 0; $i < 3; $i++) {
                $images = new Images();

                $images
                    ->setNom($faker->words(3, true))
                    ->setEnImages($faker->randomElement([true, false]))
                    ->setFile('placeholder.jpg')
                    ->addService($services)
                    ->setDescription($faker->text())
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setSlug($faker->slug())
                    ->setUser($user);

                $manager->persist($images);
            }
        }

        // MAJ 15 Avril

        //création d'UN service POUR LA PARTIE TEST CODE COVERAGE (dans la DB de TEST)

        $services = new Services();
        $services
            ->setNom('service TEST')
            ->setDescription($faker->words(10, true))
            ->setSlug('service-TEST');

        $manager->persist($services);

        //insert d'UNE image POUR LA PARTIE TEST CODE COVERAGE (dans la DB de TEST)

        $images = new Images();

        $images
            ->setNom('image TEST')
            ->setEnImages($faker->randomElement([true]))
            ->setFile('placeholder.jpg')
            ->addService($services)
            ->setDescription($faker->text())
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setSlug('image-TEST')
            ->setUser($user);

        $manager->persist($images);

        $manager->flush();
    }
}
