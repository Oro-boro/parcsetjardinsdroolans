# ParcsetJardinsDroolans

Site TFE : ParcsetJardinsDroolans

## Update 08/05 :

- Travail actuellement en local sur la V2 du projet, nettoyage / réécriture du code actuel qui à force d'essais / erreurs était devenu dégueulasse. Prochaine MAJ une fois arrivé au même niveau sur la V2.

## Fait :
- Template basique (priorité aux fonctionnalités).
- Toutes les pages sont accessibles.
- Favicon personnalisé de l'entreprise.
- Recréation du Logo de l'entreprise en haute résolution.
- Base de donnée.
- Password hash + Token.
- Recherche des services + image correspondante et leurs description dans la DB.
- Affichage des services, image + description via la DB.
- Acces au service désiré + affichage des images correspondantes à ce service via selection dans la DB (slugs).
- Gallerie "en Images" ne reprends que les images taggées selon une selection via boolean en DB.
- Formulaire "contact" fonctionnel (template a revoir).
- Mails, noms et messages stockés en DB.
- Envoi de mails / messages fonctionnel (en tout cas via MailCatcher!).
- Pipeline de tests "securité + code correction" fonctionnel (Test au commit GitLab).
- Interface Admin.
- Connection à l'interface admin via formulaire.
- Upload des images via interface Admin.
- Refonte de la navigation.


## A faire :
- Selection des images de la gallerie en 2 par 2, "avant", "apres".
- Esthetique du site (couleur, typo, animation) - En cours.
- Check de l'aspect "responsive" -En cours.
- Le blabla RGPD.
- Autres (?)

## Update 20/04
- Partie Admin - fonctionnalités : terminée à 80%
  -> La pipeline FAIL.
- Donc pas de commit, je préfère essayer de regler le souci plutôt que de push des conneries.
- Bon, Si ca n'avance pas, je devrais me résigner à laisser tomber les tests pour avancer malgré tout..

## Update 28/04

- Pipeline tests unitaires et fonctionnels Fixée. 
- Debut des feuilles de style.
- Index.html ok à 90% - responsive. Les autres pages se baseront sur cette premiere. 
- Nav intégrée et définitive (normalement ^^) - responsive
- Footer intégré mais soumis à changements (fond et forme, rgpd, copyrigt, style css etc..) - responsive


-----------------------------------------------------------------------------------------------------------------

## Mails stockés en DB : 
## RAPPEL - "cron" (quel mot pourri!) sur le serveur -> phase de Prod.
  -> Command : symfony console app:send-contact


